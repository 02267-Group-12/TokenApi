package dk.group12;

import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.jboss.logging.Logger;

import com.google.gson.Gson;

import dk.group12.events.CorrelationId;
import dk.group12.events.Event;
import dk.group12.events.TokenRequest;
import io.smallrye.mutiny.Uni;
import io.vertx.core.json.JsonObject;

@ApplicationScoped
public class TokenResource {

    private static final Logger LOG = Logger.getLogger(TokenResource.class);

    @Inject
    TokenService tService;

    /**
     * @author Jonas
     * @param jObj
     * @return
     */
    @Incoming("request-tokens")
    @Outgoing("tokens-created")
    public Uni<JsonObject> requestTokens(JsonObject jObj) {
        Event ev = convertJsonToObject(jObj, Event.class);

        TokenRequest tokenRequest = ev.getArgument(0, TokenRequest.class);

        CorrelationId corrId = ev.getArgument(1, CorrelationId.class);

        return Uni.createFrom().item(tokenRequest)
                .map(tService::registerTokens)
                .map(tokens -> new Event("tokens-retrieved", new Object[] { tokens, corrId })).log()
                .map(this::convertObjectToJson);
        // .map(jObj -> Message.of(jObj, Metadata.of(finalMeta)).withAck(obj.getAck()));
    }

    /**
     * @author Jonas
     * @param jObj
     * @return
     */
    @Incoming("request-retrieve-tokens")
    @Outgoing("tokens-retrieved")
    public Uni<JsonObject> getTokens(JsonObject jObj) {
        Event ev = convertJsonToObject(jObj, Event.class);
        UUID userUUID = ev.getArgument(0, UUID.class);
        CorrelationId corrId = ev.getArgument(1, CorrelationId.class);

        LOG.info(jObj);
        LOG.info(ev.toString());
        LOG.infof("Received Request to get Tokens from: %s", userUUID);
        LOG.infof("\t With correlationId of: %s", corrId.getId());

        return Uni.createFrom().item(userUUID)
                .map(tService::getTokens)
                .map(tokens -> new Event("tokens-retrieved", new Object[] { tokens, corrId })).log()
                .map(this::convertObjectToJson);
    }

    /**
     * @author Chris
     * @param jObj
     * @return
     */
    @Incoming("use-token")
    @Outgoing("accountUUID-retrieved")
    public Uni<JsonObject> getAccountUUID(JsonObject jObj) {
        Event ev = convertJsonToObject(jObj, Event.class);
        UUID token = ev.getArgument(0, UUID.class);
        CorrelationId corrId = ev.getArgument(1, CorrelationId.class);

        return Uni.createFrom().item(token)
                .map(tService::useToken)
                .map(accountUUID -> new Event("retrieved-accountUUID", new Object[] { accountUUID, corrId })).log()
                .map(this::convertObjectToJson);

    }

    private <T> T convertJsonToObject(JsonObject jObj, Class<T> classType) {
        return new Gson().fromJson(jObj.toString(), classType);
    }

    private JsonObject convertObjectToJson(Object obj) {
        return new JsonObject(new Gson().toJson(obj, obj.getClass()));
    }
}