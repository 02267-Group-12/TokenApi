package dk.group12.events;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class TokenRequest {
    public UUID accountUUID;
    public int amount;
}