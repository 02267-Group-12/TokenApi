package dk.group12;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Singleton;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.math.BigInteger;
import org.jboss.logging.Logger;

import dk.group12.events.TokenRequest;

@ApplicationScoped
public class TokenService {

    // private static final Logger LOG = Logger.getLogger(AccountResource.class);

    // Tokens, AccountId
    final Map<UUID, UUID> tokens = new ConcurrentHashMap<>();
    // AccountId, Set(Tokens)
    final Map<UUID, Set<UUID>> unused = new ConcurrentHashMap<>();

    /**
     * @author Jonas
     * @param request
     * @return
     */
    public Set<UUID> registerTokens(TokenRequest request) {
        // If the amount is less then 1 or greater then 5, throw an error
        if (request.amount < 1 || 5 < request.amount) {
            request.amount = Math.max(5, Math.min(1, request.amount));

        }

        final Set<UUID> accountTokens = unused.getOrDefault(request.accountUUID, new HashSet<>());
        // If there are more then 1 unused token throw an error
        if (1 < accountTokens.size()) {
            return accountTokens;
        }

        for (short i = 0; i < request.amount; ++i) {
            // Continue to generate new tokens until
            UUID newToken;
            do {
                newToken = UUID.randomUUID();
            } while (tokens.containsKey(newToken));

            accountTokens.add(newToken);
            tokens.put(newToken, request.accountUUID);
        }
        unused.put(request.accountUUID, accountTokens);
        return accountTokens;
    }

    /**
     * @author Jonas
     * @param token
     * @throws Exception
     */
    public UUID useToken(UUID token) {
        final UUID accountUUID = getAccountUUID(token);
        // If the token does not exists, then report an error
        if (accountUUID == null) {
            return null;
        }

        final var accountTokens = unused.get(accountUUID);

        // If the token could not be removed, then
        if (!accountTokens.remove(token)) {
            return null;
        }
        return accountUUID;
    }

    /**
     * @author Jonas
     * @param userUUID
     * @return
     */
    public Set<UUID> getTokens(UUID userUUID) {
        return unused.getOrDefault(userUUID, new HashSet<>());
    }

    /**
     * @author Chris
     * @param token
     * @return
     */
    public UUID getAccountUUID(UUID token) {
        return tokens.get(token);
    }

}