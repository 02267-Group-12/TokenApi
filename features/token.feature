Feature: Tokens

    Scenario: Request 1 Tokens when they have 1 active token
        Given "Marie" has 1 active tokens
        When "Marie" requests 1 tokens
        Then "Marie" has 2 active tokens

    Scenario: Request 5 Tokens when they have 1 active token
        Given "Erik" has 1 active tokens
        When "Erik" requests 5 tokens
        Then "Erik" has 6 active tokens

    Scenario: Request 1 Tokens when they have 0 active tokens
        Given "Jonas" has 0 active tokens
        When "Jonas" requests 1 tokens
        Then "Jonas" has 2 active tokens

    Scenario: Request 5 Tokens when they have 0 active tokens
        Given "Bogdan" has 0 active tokens
        When "Bogdan" requests 5 tokens
        Then "Bogdan" has 6 active tokens
        
    Scenario: Request 1 Tokens when they have 2 active tokens
        Given "Valgeir" has 2 active tokens
        When "Valgeir" requests 1 tokens
        Then "Valgeir" has 2 active tokens

    Scenario: Request 6 Tokens when they have 1 active token
        Given "Christopher" has 1 active tokens
        When "Christopher" requests 6 tokens
        Then "Christopher" has 1 active tokens

    Scenario: Request -1 Tokens when they have 1 active token
        Given "Marie" has 1 active tokens
        When "Marie" requests -1 tokens
        Then "Marie" has 1 active tokens

    Scenario: Request 0 Tokens when they have 1 active token
        Given "Erik" has 1 active tokens
        When "Erik" requests 0 tokens
        Then "Erik" has 1 active tokens

    Scenario: Request 1 Tokens when they dont exists
        When "Jonas" requests 1 tokens
        Then System throws and error
