image_name="token-api"
version=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)

docker build -t ${image_name}:${version} -t ${image_name}:latest .
